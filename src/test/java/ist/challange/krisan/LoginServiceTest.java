package ist.challange.krisan;

import ist.challange.krisan.constant.ErrorCode;
import ist.challange.krisan.entity.User;
import ist.challange.krisan.model.BaseResponse;
import ist.challange.krisan.service.LoginServiceImpl;
import ist.challange.krisan.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = LoginServiceImpl.class)
public class LoginServiceTest {

    @Autowired
    private LoginServiceImpl service;

    @MockBean
    private UserService userService;

    @Test
    public void login_EmptyUserOrPassword() {
        var result = service.login("test", "");
        var body = (BaseResponse) result.getBody();
        assert body != null;
        Assert.assertEquals(ErrorCode.USERNAME_PASSWORD_EMPTY.getMessage(), body.getStatus().getDescription());
    }

    @Test
    public void login_Success() {
        User user = new User();
        user.setUsername("test");
        user.setPassword("password");
        when(userService.getUser(anyString())).thenReturn(user);
        when(userService.isPasswordMatches(anyString(), anyString())).thenReturn(true);
        var result = service.login("test", "password");
        var body = (BaseResponse) result.getBody();
        assert body != null;
        Assert.assertEquals(ErrorCode.SUCCESS.getMessage(), body.getStatus().getDescription());
    }

    @Test
    public void login_UserNotFound() {
        when(userService.getUser(anyString())).thenReturn(null);
        var result = service.login("test", "asdfasdf");
        var body = (BaseResponse) result.getBody();
        assert body != null;
        Assert.assertEquals(ErrorCode.LOGIN_FAILED.getMessage(), body.getStatus().getDescription());
    }

    @Test
    public void login_WrongPassword() {
        User user = new User();
        user.setUsername("test");
        user.setPassword("password");
        when(userService.getUser(anyString())).thenReturn(user);
        var result = service.login("test", "password");
        var body = (BaseResponse) result.getBody();
        assert body != null;
        Assert.assertEquals(ErrorCode.LOGIN_FAILED.getMessage(), body.getStatus().getDescription());
    }


}

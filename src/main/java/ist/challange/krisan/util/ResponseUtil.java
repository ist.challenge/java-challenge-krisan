package ist.challange.krisan.util;

import ist.challange.krisan.constant.ErrorCode;
import ist.challange.krisan.model.BaseResponse;
import ist.challange.krisan.model.Status;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;

@SuppressWarnings("all")
public class ResponseUtil {

    private ResponseUtil() {}

    public static <T extends Serializable> ResponseEntity<Object> buildResponse(ErrorCode errorCode, T data, HttpStatus httpStatus) {
        return new ResponseEntity(buildResponse(errorCode, data), httpStatus);
    }

    public static <T extends Serializable> BaseResponse buildResponse(ErrorCode errorCode, T data) {
        return BaseResponse.builder()
                .status(Status.builder().code(errorCode.getCode()).description(errorCode.getMessage()).build())
                .data(data)
                .build();
    }

    public static <T extends Serializable> ResponseEntity<Object> buildResponse(String code, String message, T data, HttpStatus httpStatus) {
        return new ResponseEntity(buildResponse(code, message, data), httpStatus);
    }

    public static <T extends Serializable> BaseResponse buildResponse(String code, String message, T data) {
        return BaseResponse.builder()
                .status(Status.builder().code(code).description(message).build())
                .data(data)
                .build();
    }

}

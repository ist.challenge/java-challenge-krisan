package ist.challange.krisan.service;

import ist.challange.krisan.entity.User;
import ist.challange.krisan.model.UserDto;

import java.util.List;

public interface UserService {

    User getUser(String username);
    void saveUser(UserDto userDto);
    User updateUser(User user, UserDto userDto);
    List<User> getAllUser();
    boolean isPasswordMatches(String encodedPass, String password);

}

package ist.challange.krisan.service;

import ist.challange.krisan.entity.User;
import ist.challange.krisan.model.UserDto;
import ist.challange.krisan.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public User getUser(String username) {
        return userRepository.findByUsername(username).orElse(null);
    }

    @Override
    public void saveUser(UserDto userDto) {
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userRepository.save(user);
    }

    @Override
    public User updateUser(User user, UserDto userDto) {
        log.info("Start update user : {}", user.getUsername());
        if (null != userDto.getUsername() && null != userDto.getPassword()) {
            // condition update all
            log.info("condition update all");
            user.setUsername(userDto.getUsername());
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        } else if (null != userDto.getUsername() && Objects.isNull(userDto.getPassword())) {
            // condition update username
            log.info("condition update username only");
            user.setUsername(userDto.getUsername());
        } else {
            // condition update password
            log.info("condition update password only");
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        }
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public boolean isPasswordMatches(String encodedPass, String password) {
        return passwordEncoder.matches(password, encodedPass);
    }

}

package ist.challange.krisan.service;

import ist.challange.krisan.model.UserDto;
import org.springframework.http.ResponseEntity;

public interface LoginService {

    ResponseEntity<Object> login(String username, String password);
    ResponseEntity<Object> createUser(UserDto userDto);
    ResponseEntity<Object> updateUser(String username, UserDto userDto);
    ResponseEntity<Object> fetchUser();

}

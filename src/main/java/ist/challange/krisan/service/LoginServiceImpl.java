package ist.challange.krisan.service;

import ist.challange.krisan.constant.ErrorCode;
import ist.challange.krisan.model.UserDto;
import ist.challange.krisan.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final UserService userService;

    @Override
    public ResponseEntity<Object> login(String username, String password) {
        // validation
        if (username.isEmpty() || password.isEmpty())
            return ResponseUtil.buildResponse(ErrorCode.USERNAME_PASSWORD_EMPTY, null, HttpStatus.BAD_REQUEST);
        // check user
        var user = userService.getUser(username);
        log.info("check user: {}", !Objects.isNull(user));
        if (Objects.isNull(user))
            return ResponseUtil.buildResponse(ErrorCode.LOGIN_FAILED, null, HttpStatus.BAD_REQUEST);
        // check password
        var checkPassword = userService.isPasswordMatches(user.getPassword(), password);
        log.info("check password: {}", checkPassword);
        if (!checkPassword)
            return ResponseUtil.buildResponse(ErrorCode.LOGIN_FAILED, null, HttpStatus.BAD_REQUEST);
        return ResponseUtil.buildResponse(ErrorCode.SUCCESS, user, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> createUser(UserDto userDto) {
        var user = userService.getUser(userDto.getUsername());
        if (user != null)
            return ResponseUtil.buildResponse(ErrorCode.USER_EXIST, user, HttpStatus.CONFLICT);
        // create new user
        userService.saveUser(userDto);
        return ResponseUtil.buildResponse(ErrorCode.USER_CREATED, userDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Object> updateUser(String username, UserDto userDto) {
        // find user to be update first
        var user = userService.getUser(username);
        if (!Objects.isNull(user)) {
            if (null != userDto.getPassword()) {
                // check password if request new password
                var checkPassword = userService.isPasswordMatches(user.getPassword(), userDto.getPassword());
                log.info("check password if request new password: {}", checkPassword);
                if (checkPassword)
                    return ResponseUtil.buildResponse(ErrorCode.NEW_PASSWORD_ERROR, userDto, HttpStatus.CONFLICT);
            }
            // do update user
            user = userService.updateUser(user, userDto);
        } else {
            return ResponseUtil.buildResponse(ErrorCode.USER_NOT_FOUND, null, HttpStatus.BAD_REQUEST);
        }
        return ResponseUtil.buildResponse(ErrorCode.USER_CREATED, user, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Object> fetchUser() {
        var list = userService.getAllUser();
        return ResponseUtil.buildResponse(ErrorCode.SUCCESS, (Serializable) list, HttpStatus.OK);
    }

}

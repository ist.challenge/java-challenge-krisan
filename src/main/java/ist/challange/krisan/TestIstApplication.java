package ist.challange.krisan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestIstApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestIstApplication.class, args);
    }

}

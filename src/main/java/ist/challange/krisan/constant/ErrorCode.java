package ist.challange.krisan.constant;

import lombok.Getter;

@Getter
public enum ErrorCode {

    SUCCESS                     ("00", "success"),
    USER_EXIST                  ("409", "user sudah terpakai"),
    NEW_PASSWORD_ERROR          ("409", "password tidak boleh sama dengan password sebelumnya"),
    USERNAME_PASSWORD_EMPTY     ("400", "username atau password tidak boleh kosong"),
    LOGIN_FAILED                ("400", "username atau password salah"),
    USER_CREATED                ("201", "user berhasil disimpan"),
    USER_NOT_FOUND              ("400", "user tidak ditemukan"),
    PASSWORD_ALREADY_USED       ("400", "password baru tidak boleh sama dengan password sekarang")
    ;

    private final String code;
    private final String message;

    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

}

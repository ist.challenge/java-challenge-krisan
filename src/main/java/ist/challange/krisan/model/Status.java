package ist.challange.krisan.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Status implements Serializable {
    private static final long serialVersionUID = -8683100719482212583L;
    private String code;
    private String description;
}

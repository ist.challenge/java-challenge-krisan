package ist.challange.krisan.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BaseResponse<T extends Serializable> implements Serializable {
    private static final long serialVersionUID = 2619602082337468386L;
    private Status status;
    private T data;
}

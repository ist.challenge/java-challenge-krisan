package ist.challange.krisan.controller;

import ist.challange.krisan.model.UserDto;
import ist.challange.krisan.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class LoginController {

    private final LoginService loginService;

    @PostMapping("login")
    public ResponseEntity<Object> login(@RequestParam("username") String username, @RequestParam("password") String password) {
        return loginService.login(username, password);
    }

    @PostMapping("register")
    public ResponseEntity<Object> registration(@RequestBody UserDto userDto) {
        return loginService.createUser(userDto);
    }

    @GetMapping("user")
    public ResponseEntity<Object> fetchAllUser() {
        return loginService.fetchUser();
    }

    @PutMapping("user/update")
    public ResponseEntity<Object> updateUser(@RequestParam("username") String username, @RequestBody UserDto userDto) {
        return loginService.updateUser(username, userDto);
    }

}
